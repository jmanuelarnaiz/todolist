import React from 'react';
import MainScreen from './src/screens/main';

export default function App() {
  return <MainScreen />;
}
