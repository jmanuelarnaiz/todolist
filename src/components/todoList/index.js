import React, { Fragment } from 'react'
import { Text, View, StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  listItem: {
    borderWidth: 1,
    margin: 5,
    width: '80%',
    flexDirection: 'row',
    alignItems: 'center'
  },
  text: { padding: 5, fontWeight: 'bold' },
  textDone: {
    color: '#aaa',
    textDecorationLine: 'line-through',
    fontWeight: 'normal'
  }
})

const TodoList = ({ todos }) => {
  return (
    <Fragment>
      {todos.map(
        //todo.done ? null : <Text key={todo.text}>{todo.text}</Text>

        // !todo.done && (
        //   <View style={styles.listItem}>
        (todo) => (
          <View style={styles.listItem} key={todo.text}>
            <Text style={styles.text}>-</Text>
            <Text style={[styles.text, todo.done && styles.textDone]}>
              {todo.text}
            </Text>
          </View>
        )
      )}
    </Fragment>
  )
}

export default TodoList

// Fragment se usa cuando no quiero aplicar estilos propios ni nada, no quiero generar un nuevo DOM
// Para añadir el estilo inline: style = { { borderWidth: 1, padding: 5, margin: 5 } }

// Para aplicar varios estilos, el orden es primero es el inicial y el segundo, el que quiero que se sobreescriba.
// El de más a la derecha tiene más prioridad que los de la izquierda.

// todo.done ? styles.text : styles.textDone es equivalente a todo.done &&styles.textDone ,
// porque ya está aplicando styles.text
