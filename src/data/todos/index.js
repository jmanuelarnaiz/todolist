const getTodos = () => [
  { text: 'Task1', done: false },
  { text: 'Task2', done: false },
  { text: 'Task5', done: false },
  { text: 'Task4', done: true },
  { text: 'Task47', done: false },
];

export { getTodos };
