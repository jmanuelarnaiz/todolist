import React, { Component } from 'react'
import { SafeAreaView, View, Text, StyleSheet } from 'react-native'
// import TodoList from '../../components/todoList/index';
import TodoList from 'todoList/src/components/todoList'
import { getTodos } from 'todoList/src/data/todos'
// Es una nueva forma, añadiendo el name en el package

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop: 30,
    alignItems: 'center'
  },
  title: {
    fontWeight: 'bold',
    fontSize: 20
  }
})

class MainScreen extends Component {
  constructor(props) {
    super(props)

    this.state = {
      todos: []
    }
  }

  componentDidMount = () => {
    this.setState({ todos: getTodos() })
  }
  render() {
    //   const { todos } = this.state;
    return (
      <SafeAreaView style={styles.container}>
        <Text selectable style={styles.title}>
          ToDo List App
        </Text>
        <TodoList todos={this.state.todos} />
      </SafeAreaView>
    )
  }
}

export default MainScreen

// Para iterar se usa el map, y no el forEach
